import { defineStore } from 'pinia'
import axios from 'axios'
const urlBack = import.meta.env.VITE_APP_URL_BACK;

const apiPath= (collectionId) => {
  return '/itemCollection/' + collectionId
}

const typedApiPath= (collectionId, type) => {
  return apiPath(collectionId) + '/' + type
}

function putCollection(collections, collection) {
  const indexCollection = collections.findIndex((col) => col.collectionId === collection.collectionId)
  if (indexCollection < 0) {
    collections.push((collection))
  } else {
    collections.splice(indexCollection, 1, collection)
  }
}

export const useCollectionsStore = defineStore(
  'collections',
  {
  state: () => ({
    collections: []
  }),
  getters: {
    getByCollectionId: (state) => {
      return (collectionId) => state.collections.find((co) => co.collectionId === collectionId)
    },
  },
  actions: {
    async fetchCollection(_collectionId) {
      await  axios
        .get(
          urlBack + apiPath(_collectionId),
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        })
    },
    async removeItem(_collectionId, _itemToDeleteId) {
      await axios
        .delete(
          urlBack + apiPath(_collectionId) +
          '/item/' + _itemToDeleteId
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },

    // #### SIMPLE ITEM #####
    async putSimpleItem(_collectionId, itemToCreate, order) {
      await axios
        .post(
          urlBack + typedApiPath(_collectionId, "simpleItem") +
          "/order/" + order,
          itemToCreate
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    async updateSimpleItem(_collectionId, itemToUpdateId, itemToUpdateFrom) {
      await axios
        .put(
          urlBack + typedApiPath(_collectionId, "simpleItem") +
          '/' + itemToUpdateId,
          itemToUpdateFrom
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    async checkSimpleItem(_collectionId, itemId) {
      await axios
        .put(
          urlBack + typedApiPath(_collectionId, "simpleItem") +
          "/check/" + itemId
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    async uncheckSimpleItem(_collectionId, itemId) {
      await axios
        .put(
          urlBack + typedApiPath(_collectionId, "simpleItem") +
          "/uncheck/" + itemId
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    // #### GROUP ITEM #####
    async putGroupItem(_collectionId, itemToCreate, order) {
      await axios
        .post(
          urlBack + typedApiPath(_collectionId, "groupItem") +
          "/order/" + order,
          itemToCreate
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    async updateGroupItem(_collectionId, itemToUpdateId, itemToUpdateFrom) {
      await axios
        .put(
          urlBack + typedApiPath(_collectionId, "groupItem") +
          '/' + itemToUpdateId,
          itemToUpdateFrom
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    // #### COUNTER ITEM #####
    async putCounterItem(_collectionId, itemToCreate, order) {
      await axios
        .post(
          urlBack + typedApiPath(_collectionId, "counterItem") +
          "/order/" + order,
          itemToCreate
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
    async updateCounterItem(_collectionId, itemToUpdateId, itemToUpdateFrom) {
      await axios
        .put(
          urlBack + typedApiPath(_collectionId, "counterItem") +
          '/' + itemToUpdateId,
          itemToUpdateFrom
        )
        .then((r) => {
          putCollection(this.collections, r.data)
        });
    },
  },
})
