import { defineStore } from 'pinia'
import axios from 'axios'
const apiPath = '/checklist'
const urlBack = import.meta.env.VITE_APP_URL_BACK;

export const useChecklistStore = defineStore(
  'checklist',
  {
  state: () => ({
    checklists: []
  }),
  actions: {
    createChecklist(checklist) {
      return axios
        .post(
          urlBack + apiPath,
          checklist
        )
        .then((response) => {
          this.checklists.push(response.data)
        })
    },
    updateChecklist(_toUpdate) {
      return axios
        .put(
          urlBack + apiPath + `/${_toUpdate.id}`,
          _toUpdate
        )
        .then(
          (response) => {
            const indexToUpdate = this.checklists.findIndex((checklist) => checklist.id === _toUpdate.id)
            if (indexToUpdate >= 0) {
              this.checklists.splice(indexToUpdate, 1, response.data)
            }
          }
        )
    },
    fetchChecklists() {
      return axios
        .get(
          urlBack + apiPath
        )
        .then((response) => {
          this.checklists =  response.data
        })
    },
    deleteChecklist(checklistId) {
      return axios
        .delete(
          urlBack + apiPath +`/${checklistId}`
        )
        .then(
          () => {
            const indexToUpdate = this.checklists.findIndex((checklist) => checklist.id === checklistId)
            if (indexToUpdate >= 0) {
              this.checklists.splice(indexToUpdate, 1)
            }
          }
        )
    },
  },
  getters: {
    getByChecklistId: (state) => {
      return (checklistId) => state.checklists.find((ck) => ck.id === checklistId)
    },
  },
})
