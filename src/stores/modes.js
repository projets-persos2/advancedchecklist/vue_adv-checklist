import
{ defineStore } from 'pinia'

export const useModesStore = defineStore(
  'modes',
  {
  state: () => ({
    fastMode: false,
    mainMode: "HYBRID",
    fastModeFrequency: 5000
  }),
  actions: {
    flipFastMode() {
      this.fastMode = !this.fastMode
    },
    setToHybrid() {
      this.mainMode = "HYBRID"
    },
    setToEditing() {
      this.mainMode = "EDIT"
    },
    setToViewing() {
      this.mainMode = "VIEWING"
    },
    setFastModeFrequency(frequency) {
      this.fastModeFrequency = frequency < 1000 ? 1000: frequency
    },
  }
})
