import { defineStore } from 'pinia'
import axios from 'axios'
const apiPath = '/item/'
const urlBack = import.meta.env.VITE_APP_URL_BACK;

export const useCurrentItemStore = defineStore(
  'currentItem',
  {
  state: () => ({
    currentItem: {}
  }),
  actions: {
    fetchItem(itemId) {
      return axios
        .get(
          urlBack + apiPath + itemId
        )
        .then((response) => {
          this.currentItem =  response.data
        })
    },
  },
})
