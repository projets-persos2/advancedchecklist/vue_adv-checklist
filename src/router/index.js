import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import TestView from '@/views/TestView.vue'
import CollectionView from '@/views/GroupItemDetailView.vue'
import NewChecklistView from '@/views/ChecklistView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/checklist/:checklistId',
      name: 'checklist',
      component: NewChecklistView,
      props: true,
      children: [
        {
          path: 'group/:itemId',
          name: 'group',
          component: CollectionView,
          props: true,
        },
      ]
    },
    {
      path: '/test',
      name: 'test',
      component: TestView
    },
    {
      path: '/test',
      name: 'test',
      component: TestView,
      props: true
    },
  ]
})

export default router
