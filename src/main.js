import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import '@mdi/font/css/materialdesignicons.css'
import { createPinia } from 'pinia'
// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as labsComponents from 'vuetify/labs/components'
import * as directives from 'vuetify/directives'


const latte = {
  dark: false,
  colors: {
    background: '#eff1f5',
    surface: '#e6e9ef',
    primary: '#179299',
    'primary-lighten-1': '#1DBBC3',
    'primary-darken-1': '#13777C',
    secondary: '#fe640b',
    'secondary-lighten-1': '#FE7E34',
    'secondary-darken-1': '#DF5301',
    error: '#e64553',
    info: '#04a5e5',
    success: '#40a02b',
    warning: '#fe640b',
  },
}
//https://coolors.co/1e1e2e-5dbde9-313244-a5daf3-74c7ec-fccdb1-fab387-f89c62
const mocha = {
  dark: true,
  colors: {
    background: '#1e1e2e',
    surface: '#313244',
    'surface-lighten-1': '#45465F',
    'surface-lighten-2': '#565776',
    primary: '#74C7EC',
    'primary-lighten-1': '#A5DAF3',
    'primary-darken-1': '#5DBDE9',
    secondary: '#FAB387',
    'secondary-lighten-1': '#FCCDB1',
    'secondary-darken-1': '#F89C62',
    error: '#f38ba8',
    info: '#b4befe',
    success: '#a6e3a1',
    warning: '#f9e2af',
  },
}

createApp(App)
  .use(router)
  .use(createPinia())
  .use(createVuetify({
    components: {
      ...labsComponents,
      ...components
    },
    directives,
    theme: {
      defaultTheme: 'mocha',
      themes: {
        latte,
        mocha
      },
    },

  }))
  .mount('#app')
